# shared repo 
lots of files have absolute paths  :-(  

but this repo has some useful things to share:  
- processing models and scripts
- python expressions
- symbology-style.db


## Usage
in QGIS go to Settings...User Profiles... Open Active Profile Folder

this takes you to:
- in Linux:  `~/.local/share/QGIS/QGIS3/profiles/default/`
- in MacOS: ??
- in Windows: ??

Open a terminal one directory up from there:
- in Linux:  `cd ~/.local/share/QGIS/QGIS3/profiles/`


```sh
git clone git@gitlab.com:onezoomin/geo/qgis-shared-profile.git && cd qgis-shared-profile && git branch $USER
```

I add the branch, to ensure that you won't try to push changes to trunk.
This also means that you need to merge changes from trunk into your own branch if you want them.