layers = QgsProject.instance().mapLayers().values()
for layer in layers:
    # get the first feature's UNIT_NAME value
    layer.setName(layer.name().replace("gis_osm_", 'Portugal.OSM.'))